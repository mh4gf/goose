module cmd

go 1.17

require (
	bitbucket.org/mh4gf/goose v0.0.0-20150115234039-8488cc47d90c
	github.com/go-sql-driver/mysql v1.6.0
	github.com/kylelemons/go-gypsy v1.0.0
	github.com/lib/pq v1.10.3
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/ziutek/mymysql v1.5.4
)
